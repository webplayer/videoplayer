var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    context: __dirname + "/slon",
    
    // entry: {
    //     'slon.js': "./src/main.js",
    //     'slon.css': "./css/slon.less"
    // },
    entry: "./src/main.js",
    output: {
        path: __dirname + "/dist",
        filename: "slon.js",
        sourceMapFilename: 'slon.map',
        library: 'slon',
        libraryTarget: 'var',
    },

    devtool: {
        'inline-source-map': true
    },
    
    
    cache: true,

    module: {
        // preLoaders: [
        //   {
        //     test: /\.js$/,
        //     loader: "source-map-loader"
        //   }
        // ],
        loaders: [
            // Extract JS and Coffee files
            {
                test: /\.coffee/,
                loader: "coffee-loader"
            },

            // Extract css files
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader")
                // loader: "style-loader!css-loader"
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
                // loader: "style-loader!css-loader!less-loader"
            },

            // Fonts
            { 
                test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
                loader: "url-loader?limit=10000&minetype=application/font-woff" 
            },
            { 
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
                loader: "file-loader" 
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin("[name].css")
    ],

    resolve: {
        extensions: ['', '.coffee', '.js'],
        alias: {
            slon: __dirname + "/slon/src",
            styles: __dirname + "/slon/css"
        },
    }
};