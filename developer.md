
# Installing dependencies

In first line you need to install node.js and npm.
Then install build environment:
	
	sudo npm install webpack -g
	sudo npm install webpack-dev-server -g

	npm install webpack --save-dev
	npm install style-loader css-loader less-loader file-loader url-loader source-map-loader --save
	npm install extract-text-webpack-plugin --save
	

# Building and running

To make continiously build just run this command in project directory

	webpack --progress --colors --watch

or run devserver with command

	webpack-dev-server --progress --colors

