import shlex
import shutil
import subprocess
import sys
import time
import random
import tarfile
import tempfile
import fnmatch
import os


CURSORS = [
    '|/-\\',    # Spinning line
    # ['[o----]', '[-o---]', '[--o--]', '[---o-]', '[----o]', '[-----]'],  # Paff
]


class MXMLC(object):
    def __init__(self, mxmlc_bin):
        self.mxmlc = mxmlc_bin
        self.properties = {
            'actionscript-file-encoding': 'UTF-8',
            'accessible': False,
            'debug': False,
            'incremental': False,
            'benchmark': True,
            'use-network': True,
            'default-background-color': 0x000000,
            'default-script-limits': '1000 60',
            'language': 'EN',
            'default-size': '640 360',
            'default-frame-rate': 30,
            'static-link-runtime-shared-libraries': True,
        }
        self.working_dir = '.'
        self.defines = {}
        self.libraries = []

    def set_woring_dir(self, working_dir):
        self.working_dir = working_dir
    
    def define(self, key, value):
        self.defines[key] = value
    
    @property
    def title(self):
        return self.properties['title'] if 'title' in self.properties else None
    
    @title.setter
    def title(self, value):
        self.properties['title'] = value
        
    @property
    def publisher(self):
        return self.properties['publisher'] if 'publisher' in self.properties else None

    @publisher.setter
    def publisher(self, value):
        self.properties['publisher'] = value
    
    @property
    def description(self):
        return self.properties['description'] if 'description' in self.properties else None

    @description.setter
    def description(self, value):
        self.properties['description'] = value
    
    @property
    def creator(self):
        return self.properties['creator'] if 'creator' in self.properties else None

    @creator.setter
    def creator(self, value):
        self.properties['creator'] = value
    
    def add_lib(self, path):
        self.libraries.append(path)

    def add_all_libs_from(self, path):
        for root, dirnames, filenames in os.walk(path):
            for filename in fnmatch.filter(filenames, '*.swc'):
                self.add_lib(os.path.join(path, filename))
    
    def _get_command_line(self):
        def py_to_cmd(value):
            if isinstance(value, bool):
                return 'true' if value else 'false'
            elif isinstance(value, int):
                return '%i' % value
            else:
                return '"%s"' % value
        
        def prop_to_cmd(prop):
            many_args = ('default-script-limits', 'default-size',)
            if isinstance(prop[1], str):
                if ' ' in prop[1] and prop[0] in many_args:
                    return '-%s=%s' % (prop[0], ','.join(prop[1].split()))
            return '-%s=%s' % (prop[0], py_to_cmd(prop[1]))
            
        command = [prop_to_cmd(prop) for prop in self.properties.items()] + \
            ['-define=%s,%s' % (prop[0], py_to_cmd(prop[1])) for prop in self.defines.items()] + \
            ['-library-path+=%s' % lib for lib in self.libraries]
        
        if self.working_dir:
            command.append('-source-path=%s' % self.working_dir)
        
        return command
    
    def __del__(self):
        if hasattr(self, '_tmp'):
            self._tmp.close()
    
    def compile(self, entry_point, suffix='.swf'):
        tmp = tempfile.NamedTemporaryFile(suffix=suffix, prefix='pymxmlc-')

        command = [self.mxmlc, ] + self._get_command_line()
        command.append('-o=%s' % tmp.name)
        command.append(entry_point)
        
        p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        self._tmp = tmp
        return p
    
    def compile_swc(self, entry_point):
        return self.compile(entry_point, 'swc')
    
    def save(self, dest):
        if not self._tmp:
            raise Exception('Compile first')
        with open(dest, 'wb') as f:
            f.write(self._tmp.file.read())


def spinning_cursor(cursor_set=None):
    cursors = cursor_set or random.choice(CURSORS)
    while True:
        for cursor in cursors:
            yield cursor


def run_command(command):
    args = shlex.split(command)
    p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    return p


def waiter(process):
    spinner = spinning_cursor()
    
    while process.poll() is None:
        sys.stdout.write("\r%s" % spinner.next())
        sys.stdout.flush()
        time.sleep(.07)

    sys.stdout.write("\r")
    sys.stdout.flush()


def run_and_wait(command):
    p = run_command(command)
    waiter(p)
    return p

if __name__ == '__main__':
    build_type = 'stable'
    absolute_link_to_player = 'http://aloha.cdnvideo.ru/slon20/%s/' % build_type
    build_root = os.path.abspath('./build/%s' % build_type)
    install_root = '/opt/slon20-%s' % build_type
    version = '1.0.0'

    # Cleaning build roots
    shutil.rmtree(build_root)
    os.makedirs(build_root)

    print('Compiling JavaScript core...')
    run_and_wait('webpack --progress --optimize-minimize --optimize-occurence-order')
    
    print('Patching JavaScript core for "%s" build' % build_type)
    js_orig = open('./dist/slon.js', 'r').read()
    js_orig = js_orig.replace('GENERATED_CDN_VSN', build_type)
    os.unlink('./dist/slon.js')
    with open(os.path.join(build_root, 'slon.js'), 'w') as js_fd:
        js_fd.write(js_orig)
    
    print('Compiling Flash core...')
    m = MXMLC(os.path.abspath('./utils/flex/4.13/bin/mxmlc'))
    m.creator = 'Alexey Grechishkin'
    m.publisher = m.creator
    m.title = 'SlonJS'
    m.description = 'Flash fallback for SlonJS'

    m.define('CONFIG::LOGGING', False)
    m.define('CONFIG::VERSION', version)

    m.add_all_libs_from(os.path.abspath('./slon/flash/libs/'))
    m.working_dir = os.path.abspath('./slon/flash/src/')
    p = m.compile(os.path.join(m.working_dir, 'Slon.as'))
    waiter(p)
    m.save(os.path.join(build_root, 'slon.swf'))

    print('Compiling styles...')
    p = run_and_wait('lessc -x -O2 %s' % os.path.abspath('slon/css/slon.less'))
    css_content = p.stdout.read()
    css_content = css_content.replace('font/slon.', '%sfont/slon.' % absolute_link_to_player)
    with open(os.path.join(build_root, 'slon.css'), 'wb') as css:
        css.write(css_content)

    print('Compiling fonts...')
    if os.path.exists(os.path.join(build_root, 'font')):
        shutil.rmtree(os.path.join(build_root, 'font'))
    shutil.copytree('./slon/css/font/', os.path.join(build_root, 'font'))

    print('Compiling archive.')
    archive_path = "./build/slon-%s.tar.gz" % build_type
    os.unlink(archive_path)
    tar = tarfile.open(archive_path, "w:gz")
    # tar.add(build_root, recursive=True)
    for root, dirnames, filenames in os.walk(build_root):
        arc_path = os.path.abspath(root).replace(os.path.abspath(build_root), '')
        if arc_path.startswith('/'):
            arc_path = arc_path[1:]

        for filename in filenames:
            tar.add(os.path.join(root, filename), arcname=os.path.join(install_root, arc_path, os.path.basename(filename)))
    tar.close()

    # print('Building RPM package')
    # run_and_wait('fpm -s tar -t rpm -n slon-%(build_type)s -v %(version)s' % locals())

    print('Build complete')