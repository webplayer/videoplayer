Name:           slonjs-{{VERSION_TAG}}
Version:        {{VERSION}}
Release:        {{VERSION_MAJOR}}
Summary:        SlonJS - CDN Web Video Player

Group:          Utilities
License:        Commercial
URL:            https://z.cdnvideo.ru/projects/cdn-flash-player
Source0:        slonjs-{{VERSION_TAG}}-{{VERSION}}.tar.gz
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


#BuildRequires: 
#Requires:      

%description
SlonJS is a web video player based on JavaScript and HTML5 technologies with fallback into Flash.


%prep
%setup -q


%build


%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/opt/slonjs-{{VERSION_TAG}}
#install slonjs-{{VERSION_TAG}}.sh $RPM_BUILD_ROOT/opt/slonjs-{{VERSION_TAG}}/slonjs-{{VERSION_TAG}}.sh


%clean
rm -rf $RPM_BUILD_ROOT


%files
%dir /opt/slonjs-{{VERSION_TAG}}
%defattr(-,root,root,-)
/opt/slonjs-{{VERSION_TAG}}/slonjs-{{VERSION_TAG}}.sh

%post
chmod 755 -R /opt/slonjs-{{VERSION_TAG}}