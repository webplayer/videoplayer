/**
 * @fileoverview Main function src.
 */

// HTML5 Shiv. Must be in <head> to support older browsers.
document.createElement('video');
document.createElement('audio');
document.createElement('track');

// require('styles/slon.less');
// require('styles/beople.less');

var slon = require('slon/core');

slon.Player = require('slon/player');
slon.MediaSource = require('slon/media/mediasources/mediasource');

slon.Html5 = require('slon/media/html5');
slon.Flash = require('slon/media/flash');

// Preload components
slon.components = require('slon/component').components = {
    'MediaLoder': require('slon/media/loader'),
    'PosterImage': require('slon/gui/poster'),
    'LoadingSpinner': require('slon/gui/loading-spinner'),
    'BigPlayButton': require('slon/gui/big-play-button'),
    'ControlBar': require('slon/gui/control-bar/control-bar'),
    'ErrorDisplay': require('slon/gui/error-display'),
    'PlayToggle': require('slon/gui/control-bar/play-toggle'),
    'LiveDisplay': require('slon/gui/control-bar/live-display'),
    'FullscreenToggle': require('slon/gui/control-bar/fullscreen-toggle'),

    'CurrentTimeDisplay': require('slon/gui/control-bar/time-display/current-time-display'),
    'TimeDivider': require('slon/gui/control-bar/time-display/time-devider'),
    'DurationDisplay': require('slon/gui/control-bar/time-display/duration-display'),
    'RemainingTimeDisplay': require('slon/gui/control-bar/time-display/remaining-time-display'),

    'PlaybackRateMenuButton': require('slon/gui/control-bar/playback-rate/button'),

    'VolumeMenuButton': require('slon/gui/control-bar/volume/button'),
    'VolumeLevel': require('slon/gui/control-bar/volume/level'),
    'VolumeHandle': require('slon/gui/control-bar/volume/handle'),
    'VolumeBar': require('slon/gui/control-bar/volume/bar'),
    'VolumeControl': require('slon/gui/control-bar/volume/control'),

    'MuteToggle': require('slon/gui/control-bar/mute-toggle'),

    'ProgressControl': require('slon/gui/control-bar/progress/control'),
    'LoadProgressBar': require('slon/gui/control-bar/progress/loadbar'),
    'PlayProgressBar': require('slon/gui/control-bar/progress/playbar'),
    'SeekBar': require('slon/gui/control-bar/progress/seekbar'),
    'SeekHandle': require('slon/gui/control-bar/progress/seekhandle'),
}
slon.cache = require('slon/lib/cache');

// slon.Hls = require('slon/protocols/hls/hls');

slon.autoSetupTimeout(1);

module.exports = slon;
