/**
 * @fileoverview Externs for slon-player.swf. Externs are functions
 * that the compiler shouldn't obfuscate.
 */

/**
 * @param {string} name
 */
HTMLObjectElement.prototype.slon_getProperty = function(name) {};

/**
 * @param {string} name
 * @param {string|number} value
 */
HTMLObjectElement.prototype.slon_setProperty = function(name, value) {};

/**
 * Control methods
 */
HTMLObjectElement.prototype.slon_play = function() {};
HTMLObjectElement.prototype.slon_pause = function() {};
HTMLObjectElement.prototype.slon_load = function() {};

/**
 * @param {string} src
 */
HTMLObjectElement.prototype.slon_src = function(src) {};
