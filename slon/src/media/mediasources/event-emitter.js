EventEmitter = function() {};

EventEmitter.prototype.init = function() {
    this.listeners = [];
};

EventEmitter.prototype.addEventListener = function(type, listener) {
    if (!this.listeners[type]) {
        this.listeners[type] = [];
    }
    this.listeners[type].unshift(listener);
};

EventEmitter.prototype.removeEventListener = function(type, listener) {
    var listeners = this.listeners[type],
        i = listeners.length;
    while (i--) {
        if (listeners[i] === listener) {
            return listeners.splice(i, 1);
        }
    }
};

EventEmitter.prototype.trigger = function(event) {
    var listeners = this.listeners[event.type] || [],
        i = listeners.length;
    while (i--) {
        listeners[i](event);
    }
};

module.exports = EventEmitter;