var EventEmitter = require('slon/media/mediasources/event-emitter');
var constants = require('slon/media/mediasources/constants');


// Source Buffer
SourceBuffer = function(source) {
    var self = this,

        // byte arrays queued to be appended
        buffer = [],

        // the total number of queued bytes
        bufferSize = 0,
        append = function() {
            var chunk, i, length, payload,
                binary = '';

            if (!buffer.length) {
                // do nothing if the buffer is empty
                return;
            }

            // concatenate appends up to the max append size
            payload = new Uint8Array(Math.min(constants.MAX_APPEND_SIZE, bufferSize));
            i = payload.byteLength;
            while (i) {
                chunk = buffer[0].subarray(0, i);

                payload.set(chunk, payload.byteLength - i);

                // requeue any bytes that won't make it this round
                if (chunk.byteLength < buffer[0].byteLength) {
                    buffer[0] = buffer[0].subarray(i);
                } else {
                    buffer.shift();
                }

                i -= chunk.byteLength;
            }
            bufferSize -= payload.byteLength;

            // schedule another append if necessary
            if (bufferSize !== 0) {
                constants.requestAnimationFrame(append);
            } else {
                self.trigger({
                    type: 'updateend'
                });
            }

            // base64 encode the bytes
            for (i = 0, length = payload.byteLength; i < length; i++) {
                binary += String.fromCharCode(payload[i]);
            }
            b64str = window.btoa(binary);

            // bypass normal ExternalInterface calls and pass xml directly
            // EI can be slow by default
            self.source.swfObj.CallFunction('<invoke name="slon_appendBuffer"' + 'returntype="javascript"><arguments><string>' + b64str + '</string></arguments></invoke>');
        };

    SourceBuffer.prototype.init.call(this);
    this.source = source;

    // accept video data and pass to the video (swf) object
    this.appendBuffer = function(uint8Array) {
        if (buffer.length === 0) {
            constants.requestAnimationFrame(append);
        }

        this.trigger({
            type: 'update'
        });

        buffer.push(uint8Array);
        bufferSize += uint8Array.byteLength;
    };

    // reset the parser and remove any data queued to be sent to the swf
    this.abort = function() {
        buffer = [];
        bufferSize = 0;
        this.source.swfObj.slon_abort();
    };
};

SourceBuffer.prototype = new EventEmitter();

module.exports = SourceBuffer;