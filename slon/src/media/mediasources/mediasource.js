var EventEmitter = require('slon/media/mediasources/event-emitter');
var SourceBuffer = require('slon/media/mediasources/source-buffer');

var constants = require('slon/media/mediasources/constants');

var Player = require('slon/player');
var cache = require('slon/lib/cache');


// Media Source
var SlonMediaSource = function() {
    var self = this;
    // console.log('SlonMediaSource');

    SlonMediaSource.prototype.init.call(this);

    this.sourceBuffers = [];
    this.readyState = 'closed';
    this.listeners = {
    sourceopen: [
            function(event) {
                // find the swf where we will push media data
                self.swfObj = document.getElementById(event.swfId);
                self.readyState = 'open';

                // trigger load events
                if (self.swfObj) {
                    self.swfObj.slon_load();
                }
            }
        ],
        webkitsourceopen: [
            function(event) {
                self.trigger({
                    type: 'sourceopen'
                });
            }
        ]
    };
};
SlonMediaSource.prototype = new EventEmitter();

// create a new source buffer to receive a type of media data
SlonMediaSource.prototype.addSourceBuffer = function(type) {
    var sourceBuffer;

    // console.log('SlonMediaSource.addSourceBuffer', type);

    // if this is an FLV type, we'll push data to flash
    if (constants.flvCodec.test(type)) {
        // console.log('SlonMediaSource.addSourceBuffer', type, 'OK');
        // Flash source buffers
        sourceBuffer = new SourceBuffer(this);
    } else if (this.nativeSource) {
        // console.log('SlonMediaSource.addSourceBuffer', type, 'Native');
        // native source buffers
        sourceBuffer = this.nativeSource.addSourceBuffer.apply(this.nativeSource, arguments);
    } else {
        // console.log('SlonMediaSource.addSourceBuffer', type, 'FAIL');
        throw new Error('NotSupportedError (SlonPlayer)');
    }

    this.sourceBuffers.push(sourceBuffer);
    return sourceBuffer;
};

SlonMediaSource.prototype.endOfStream = function() {
    // console.log('SlonMediaSource.endOfStream');
    this.swfObj.slon_endOfStream();
    this.readyState = 'ended';
};


// provide a method for a swf object to notify JS that a media source is now open
SlonMediaSource.open = function(msObjectURL, swfId) {
    // console.log('SlonMediaSource.open', msObjectURL, swfId);
    var mediaSource = cache.mediaSources[msObjectURL];

    if (mediaSource) {
        mediaSource.trigger({
            type: 'sourceopen',
            swfId: swfId
        });
    } else {
        throw new Error('Media Source not found (SlonPlayer)');
    }
};


// plugin
Player.plugin('mediaSource', function(options) {
    // console.log('mediaSource', options);

    var player = this;

    player.on('loadstart', function() {
        var url = player.currentSrc(),
            trigger = function(event) {
                mediaSource.trigger(event);
            },
            mediaSource;

        if (player.techName === 'Html5' && url.indexOf(constants.objectUrlPrefix) === 0) {
            // use the native media source implementation
            mediaSource = cache.mediaSources[url];
            // console.log('mediaSource', mediaSource, cache.mediaSources);

            if (!mediaSource.nativeUrl) {
                // initialize the native source
                mediaSource.nativeSource = new constants.NativeSlonMediaSource();
                mediaSource.nativeSource.addEventListener('sourceopen', trigger, false);
                mediaSource.nativeSource.addEventListener('webkitsourceopen', trigger, false);
                mediaSource.nativeUrl = nativeUrl.createObjectURL(mediaSource.nativeSource);
            }
            player.src(mediaSource.nativeUrl);
        }
    });
});


module.exports = SlonMediaSource;
