var cache = require('slon/lib/cache');
var constants = require('slon/media/mediasources/constants');


module.exports = {
    createObjectURL: function(object) {
        var url = constants.objectUrlPrefix + constants.urlCount;

        // console.log('URL.createObjectURL', url, constants);

        constants.urlCount++;

        // setup the mapping back to object
        cache.mediaSources[url] = object;

        return url;
    }
};
