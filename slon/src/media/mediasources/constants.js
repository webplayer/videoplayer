var constants = {};


constants.objectUrlPrefix = 'blob:slon-media-source/';

constants.flvCodec = /video\/flv(;\s*codecs=["']vp6,aac["'])?$/;

constants.urlCount = 0;
constants.nativeUrl = window.URL || {};

constants.NativeMediaSource = require('slon/media/mediasources/native-media-source');
constants.requestAnimationFrame = require('slon/media/mediasources/request-animation-frame');
/**
 * The maximum size in bytes for append operations to the SLonPlayer
 * SWF. Calling through to Flash blocks and can be expensive so
 * tuning this parameter may improve playback on slower
 * systems. There are two factors to consider:
 * - Each interaction with the SWF must be quick or you risk dropping
 * video frames. To maintain 60fps for the rest of the page, each append
 * cannot take longer than 16ms. Given the likelihood that the page will
 * be executing more javascript than just playback, you probably want to
 * aim for ~8ms.
 * - Bigger appends significantly increase throughput. The total number of
 * bytes over time delivered to the SWF must exceed the video bitrate or
 * playback will stall.
 *
 * The default is set so that a 4MB/s stream should playback
 * without stuttering.
 */
constants.MAX_APPEND_SIZE = Math.ceil((4 * 1024 * 1024) / 60);


module.exports = constants;
