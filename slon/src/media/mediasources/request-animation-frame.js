/**
 * Polyfill for requestAnimationFrame
 * @param callback {function} the function to run at the next frame
 * @see https://developer.mozilla.org/en-US/docs/Web/API/window.requestAnimationFrame
 */
var requestAnimationFrame = function(callback) {
    return (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
        return window.setTimeout(callback, 1000 / 60);
    })(callback);
};

module.exports = requestAnimationFrame;