/**
 * @fileoverview Externs for slon.Player. Externs are functions that the
 * compiler shouldn't obfuscate.
 */

/**
 * @constructor
 * @extends {slon.Component}
 */
slon.Player = function(){};

/**
 * Native HTML5 video properties
 * Most likely covered by the default closure compiler externs
 * Copied list from http://code.google.com/p/closure-compiler/source/browse/externs/html5.js?spec=svne2e531de906d9ccccf23516bd2dd6152a93f6468&r=e2e531de906d9ccccf23516bd2dd6152a93f6468
 * May not all be available on a videojs player yet
 */
slon.Player.prototype.error = function(){};
slon.Player.prototype.src = function(){};
slon.Player.prototype.currentSrc = function(){};
slon.Player.prototype.networkState = function(){};
slon.Player.prototype.buffered = function(){};
slon.Player.prototype.load = function(){};
slon.Player.prototype.canPlayType = function(){};
slon.Player.prototype.readyState = function(){};
slon.Player.prototype.seeking = function(){};
slon.Player.prototype.currentTime = function(){};
slon.Player.prototype.remainingTime = function(){};
slon.Player.prototype.startTime = function(){};
slon.Player.prototype.duration = function(){};
slon.Player.prototype.paused = function(){};
slon.Player.prototype.defaultPlaybackRate = function(){};
slon.Player.prototype.playbackRate = function(){};
slon.Player.prototype.played = function(){};
slon.Player.prototype.seekable = function(){};
slon.Player.prototype.ended = function(){};
slon.Player.prototype.autoplay = function(){};
slon.Player.prototype.loop = function(){};
slon.Player.prototype.play = function() {};
slon.Player.prototype.pause = function() {};
slon.Player.prototype.controls = function(){};
slon.Player.prototype.volume = function(){};
slon.Player.prototype.muted = function(){};
slon.Player.prototype.width = function(){};
slon.Player.prototype.height = function(){};
slon.Player.prototype.videoWidth = function(){};
slon.Player.prototype.videoHeight = function(){};
slon.Player.prototype.poster = function(){};

/**
 * Fullscreen functionality
 */
slon.Player.prototype.isFullscreen = function(){};
slon.Player.prototype.isFullScreen = function(){}; /* deprecated */
slon.Player.prototype.requestFullscreen = function(){};
slon.Player.prototype.requestFullScreen = function(){}; /* deprecated */
slon.Player.prototype.exitFullscreen = function(){};
slon.Player.prototype.cancelFullScreen = function(){}; /* deprecated */

/**
 * Text tracks
 */
slon.Player.prototype.textTracks = function(){};

/**
 * Component functions
 */
slon.Player.prototype.dispose = function(){};

/**
 * Buffered percent
 */
slon.Player.prototype.bufferedPercent = function(){};

/**
 * User activity functions
 */
slon.Player.prototype.reportUserActivity = function(){};
slon.Player.prototype.userActive = function(){};

/**
 * Native controls
 */
slon.Player.prototype.usingNativeControls = function(){};

/**
 * Source selection
 */
slon.Player.prototype.selectSource = function(){};
