var DOM = require('slon/lib/dom');
var JSON = require('slon/lib/json');
var events = require('slon/lib/events');
var cache = require('slon/lib/cache');
var util = require('slon/lib/util');

var Raven = require('slon/lib/raven/raven');

/**
 * Doubles as the main function for users to create a player instance and also
 * the main library object.
 *
 * **ALIASES** videojs, _V_ (deprecated)
 *
 * The `slon` function can be used to initialize or retrieve a player.
 *
 *     var myPlayer = slon('my_video_id');
 *
 * @param  {String|Element} id      Video element or video element ID
 * @param  {Object=} options        Optional options object for config/settings
 * @param  {Function=} ready        Optional ready callback
 * @return {Player}             A player instance
 * @namespace
 */
var core = function(id, options, ready){
  var Player = require('slon/player');
  var tag; // Element of ID

  // Allow for element or ID to be passed in
  // String ID
  if (typeof id === 'string') {

    // Adjust for jQuery ID syntax
    if (id.indexOf('#') === 0) {
      id = id.slice(1);
    }

    // If a player instance has already been created for this ID return it.
    if (cache.players[id]) {
      return cache.players[id];

    // Otherwise get element for ID
    } else {
      tag = DOM.el(id);
    }

  // ID is a media element
  } else {
    tag = id;
  }

  // Check for a useable element
  if (!tag || !tag.nodeName) { // re: nodeName, could be a box div also
    throw new TypeError('The element or ID supplied is not valid. (SlonPlayer)'); // Returns
  }

  // Element may have a player attr referring to an already created player instance.
  // If not, set up a new player and return the instance.
  return tag['player'] || new Player(tag, options, ready);
};

// Extended name, also available externally, window.videojs
// var coreplayer = window['coreplayer'] = core;

// CDN Version. Used to target right flash swf.
core.CDN_VERSION = 'GENERATED_CDN_VSN';
core.ACCESS_PROTOCOL = ('https:' == document.location.protocol ? 'https://' : 'http://');


// Test for MediaSource support in browser
if( !!window.MediaSource ){
  core.MediaSource = window.MediaSource;
}

/**
 * Global Player instance options, surfaced from core.Player.prototype.options_
 * core.options = core.Player.prototype.options_
 * All options should use string keys so they avoid
 * renaming by closure compiler
 * @type {Object}
 */
core.options = require('slon/options');

// Set CDN Version of swf
// The added (+) blocks the replace from changing this GENERATED_CDN_VSN string
if (core.CDN_VERSION !== 'GENERATED'+'_CDN_VSN') {
  core.options['flash']['swf'] = core.ACCESS_PROTOCOL + 'aloha.cdnvideo.ru/slon20/'+core.CDN_VERSION+'/slon.swf';
}


/**
 * Utility function for adding languages to the default options. Useful for
 * amending multiple language support at runtime.
 *
 * Example: core.addLanguage('es', {'Hello':'Hola'});
 *
 * @param  {String} code The language code or dictionary property
 * @param  {Object} data The data values to be translated
 * @return {Object} The resulting global languages dictionary object
 */
core.addLanguage = function(code, data){
  if(core.options['languages'][code] !== undefined) {
    core.options['languages'][code] = util.mergeOptions(core.options['languages'][code], data);
  } else {
    core.options['languages'][code] = data;
  }
  return core.options['languages'];
};

// Automatically set up any tags that have a data-setup attribute
core.autoSetup = function(){
  var options, mediaEl, player, i, e;

  // One day, when we stop supporting IE8, go back to this, but in the meantime...*hack hack hack*
  // var vids = Array.prototype.slice.call(document.getElementsByTagName('video'));
  // var audios = Array.prototype.slice.call(document.getElementsByTagName('audio'));
  // var mediaEls = vids.concat(audios);

  // Because IE8 doesn't support calling slice on a node list, we need to loop through each list of elements
  // to build up a new, combined list of elements.
  var vids = document.getElementsByTagName('video');
  var audios = document.getElementsByTagName('audio');
  var mediaEls = [];
  if (vids && vids.length > 0) {
    for(i=0, e=vids.length; i<e; i++) {
      mediaEls.push(vids[i]);
    }
  }
  if (audios && audios.length > 0) {
    for(i=0, e=audios.length; i<e; i++) {
      mediaEls.push(audios[i]);
    }
  }

  // Check if any media elements exist
  if (mediaEls && mediaEls.length > 0) {

    for (i=0,e=mediaEls.length; i<e; i++) {
      mediaEl = mediaEls[i];

      // Check if element exists, has getAttribute func.
      // IE seems to consider typeof el.getAttribute == 'object' instead of 'function' like expected, at least when loading the player immediately.
      if (mediaEl && mediaEl.getAttribute) {

        // Make sure this player hasn't already been set up.
        if (mediaEl['player'] === undefined) {
          options = mediaEl.getAttribute('data-setup');

          // Check if data-setup attr exists.
          // We only auto-setup if they've added the data-setup attr.
          if (options !== null) {
            // Create new slonplayer instance.
            player = core(mediaEl);
          }
        }

      // If getAttribute isn't defined, we need to wait for the DOM.
      } else {
        core.autoSetupTimeout(1);
        break;
      }
    }

  // No videos were found, so keep looping unless page is finished loading.
  } else if (!core.windowLoaded) {
    core.autoSetupTimeout(1);
  }
};

// Pause to let the DOM keep processing
core.autoSetupTimeout = function(wait){
  setTimeout(core.autoSetup, wait);
};

if (document.readyState === 'complete') {
  core.windowLoaded = true;
} else {
  events.one(window, 'load', function(){
    core.windowLoaded = true;
  });
}


// if (typeof define === 'function' && define['amd']) {
//   define([], function(){ return coreplayer; });

// // checking that module is an object too because of umdjs/umd#35
// } else if (typeof exports === 'object' && typeof module === 'object') {
//   module['exports'] = coreplayer;
// }


core.Raven = Raven;
core.Raven.config('http://d4d9c95c7e094b708921a8dd7ac34b5e@sentry.alexsnet.ru/15', {
    ignoreErrors: [
      // Random plugins/extensions
      'top.GLOBALS',
      // See: http://blog.errorception.com/2012/03/tale-of-unfindable-js-error. html
      'originalCreateNotification',
      'canvas.contentDocument',
      'MyApp_RemoveAllHighlights',
      'http://tt.epicplay.com',
      'Can\'t find variable: ZiteReader',
      'jigsaw is not defined',
      'ComboSearch is not defined',
      'http://loading.retry.widdit.com/',
      'atomicFindClose',
      // Facebook borked
      'fb_xd_fragment',
      // ISP "optimizing" proxy - `Cache-Control: no-transform` seems to reduce this. (thanks @acdha)
      // See http://stackoverflow.com/questions/4113268/how-to-stop-javascript-injection-from-vodafone-proxy
      'bmi_SafeAddOnload',
      'EBCallBackMessageReceived',
      // See http://toolbar.conduit.com/Developer/HtmlAndGadget/Methods/JSInjection.aspx
      'conduitPage'
    ],
    ignoreUrls: [
      // Facebook flakiness
      /graph\.facebook\.com/i,
      // Facebook blocked
      /connect\.facebook\.net\/en_US\/all\.js/i,
      // Woopra flakiness
      /eatdifferent\.com\.woopra-ns\.com/i,
      /static\.woopra\.com\/js\/woopra\.js/i,
      // Chrome extensions
      /extensions\//i,
      /^chrome:\/\//i,
      // Other plugins
      /127\.0\.0\.1:4001\/isrunning/i,  // Cacaoweb
      /webappstoolbarba\.texthelp\.com\//i,
      /metrics\.itunes\.apple\.com\.edgesuite\.net\//i
    ]
}).install();

module.exports = core;
