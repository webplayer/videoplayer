var obj = require('slon/lib/obj');

/**
 * Array functions container
 * @type {Object}
 * @private
 */
var arr = {};

/*
 * Loops through an array and runs a function for each item inside it.
 * @param  {Array}    array       The array
 * @param  {Function} callback    The function to be run for each item
 * @param  {*}        thisArg     The `this` binding of callback
 * @returns {Array}               The array
 * @private
 */
arr.forEach = function(array, callback, thisArg) {
  if (obj.isArray(array) && callback instanceof Function) {
    for (var i = 0, len = array.length; i < len; ++i) {
      callback.call(thisArg || slon, array[i], i, array);
    }
  }

  return array;
};

module.exports = arr;