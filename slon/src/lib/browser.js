var browser = {};

browser.fullscreenAPI = require('slon/lib/fullscreen-api');

/**
 * Useragent for browser testing.
 * @type {String}
 * @constant
 * @private
 */
browser.USER_AGENT = navigator.userAgent;

/**
 * Device is an iPhone
 * @type {Boolean}
 * @constant
 * @private
 */
browser.IS_IPHONE = (/iPhone/i).test(browser.USER_AGENT);
browser.IS_IPAD = (/iPad/i).test(browser.USER_AGENT);
browser.IS_IPOD = (/iPod/i).test(browser.USER_AGENT);
browser.IS_IOS = browser.IS_IPHONE || browser.IS_IPAD || browser.IS_IPOD;

browser.IOS_VERSION = (function(){
  var match = browser.USER_AGENT.match(/OS (\d+)_/i);
  if (match && match[1]) { return match[1]; }
})();

browser.IS_ANDROID = (/Android/i).test(browser.USER_AGENT);
browser.ANDROID_VERSION = (function() {
  // This matches Android Major.Minor.Patch versions
  // ANDROID_VERSION is Major.Minor as a Number, if Minor isn't available, then only Major is returned
  var match = browser.USER_AGENT.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),
    major,
    minor;

  if (!match) {
    return null;
  }

  major = match[1] && parseFloat(match[1]);
  minor = match[2] && parseFloat(match[2]);

  if (major && minor) {
    return parseFloat(match[1] + '.' + match[2]);
  } else if (major) {
    return major;
  } else {
    return null;
  }
})();
// Old Android is defined as Version older than 2.3, and requiring a webkit version of the android browser
browser.IS_OLD_ANDROID = browser.IS_ANDROID && (/webkit/i).test(browser.USER_AGENT) && browser.ANDROID_VERSION < 2.3;

browser.IS_FIREFOX = (/Firefox/i).test(browser.USER_AGENT);
browser.IS_CHROME = (/Chrome/i).test(browser.USER_AGENT);

browser.TOUCH_ENABLED = !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch);

module.exports = browser;