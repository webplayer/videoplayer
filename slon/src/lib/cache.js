var cache = {};

/**
 * Element Data Store. Allows for binding data to an element without putting it directly on the element.
 * Ex. Event listneres are stored here.
 * (also from jsninja.com, slightly modified and updated for closure compiler)
 * @type {Object}
 * @private
 */
cache.cache = {};

/**
 * Unique ID for an element or function
 * @type {Number}
 * @private
 */
cache.guid = 1;

/**
 * Global player list
 * @type {Object}
 */
cache.players = {};

/**
 * Unique attribute name to store an element's guid in
 * @type {String}
 * @constant
 * @private
 */
cache.expando = 'vdata' + (new Date()).getTime();

/**
 * Element for testing browser HTML5 video capabilities
 * @type {Element}
 * @constant
 * @private
 */
cache.TEST_VID = NaN;

cache.media = {};

// store references to the media sources so they can be connected
// to a video element (a swf object)
cache.mediaSources = {};

module.exports = cache;