/**
 * Network Abstraction Layer (NAL) units are the packets of an H264
 * stream. NAL units are divided into types based on their payload
 * data. Each type has a unique numeric identifier.
 *
 *              NAL unit
 * |- NAL header -|------ RBSP ------|
 *
 * NAL unit: Network abstraction layer unit. The combination of a NAL
 * header and an RBSP.
 * NAL header: the encapsulation unit for transport-specific metadata in
 * an h264 stream. Exactly one byte.
 */
// incomplete, see Table 7.1 of ITU-T H.264 for 12-32
var NALUnitType = {
    unspecified: 0,
    slice_layer_without_partitioning_rbsp_non_idr: 1,
    slice_data_partition_a_layer_rbsp: 2,
    slice_data_partition_b_layer_rbsp: 3,
    slice_data_partition_c_layer_rbsp: 4,
    slice_layer_without_partitioning_rbsp_idr: 5,
    sei_rbsp: 6,
    seq_parameter_set_rbsp: 7,
    pic_parameter_set_rbsp: 8,
    access_unit_delimiter_rbsp: 9,
    end_of_seq_rbsp: 10,
    end_of_stream_rbsp: 11
};