m3util = {
    noop: function() {},

    // "forgiving" attribute list psuedo-grammar:
    // attributes -> keyvalue (',' keyvalue)*
    // keyvalue   -> key '=' value
    // key        -> [^=]*
    // value      -> '"' [^"]* '"' | [^,]*
    attributeSeparator : (function() {
        var
            key = '[^=]*',
            value = '"[^"]*"|[^,]*',
            keyvalue = '(?:' + key + ')=(?:' + value + ')';

        return new RegExp('(?:^|,)(' + keyvalue + ')');
    })(),

    parseAttributes: function(attributes) {
        var
        // split the string using attributes as the separator
            attrs = attributes.split(m3util.attributeSeparator),
            i = attrs.length,
            result = {},
            attr;

        while (i--) {
            // filter out unmatched portions of the string
            if (attrs[i] === '') {
                continue;
            }

            // split the key and value
            attr = /([^=]*)=(.*)/.exec(attrs[i]).slice(1);
            // trim whitespace and remove optional quotes around the value
            attr[0] = attr[0].replace(/^\s+|\s+$/g, '');
            attr[1] = attr[1].replace(/^\s+|\s+$/g, '');
            attr[1] = attr[1].replace(/^['"](.*)['"]$/g, '$1');
            result[attr[0]] = attr[1];
        }
        return result;
    }
};

module.exports = m3util;