var Stream = require('slon/protocols/hls/stream');
var LineStream = require('slon/protocols/hls/m3u8/linestream');
var ParseStream = require('slon/protocols/hls/m3u8/parsestream');

var util = require('slon/lib/util');
var m3util = require('slon/protocols/hls/m3u8/util');

/**
 * A parser for M3U8 files. The current interpretation of the input is
 * exposed as a property `manifest` on parser objects. It's just two lines to
 * create and parse a manifest once you have the contents available as a string:
 *
 * ```js
 * var parser = new Parser();
 * parser.push(xhr.responseText);
 * ```
 *
 * New input can later be applied to update the manifest object by calling
 * `push` again.
 *
 * The parser attempts to create a usable manifest object even if the
 * underlying input is somewhat nonsensical. It emits `info` and `warning`
 * events during the parse if it encounters input that seems invalid or
 * requires some property of the manifest object to be defaulted.
 */
var Parser = function() {
    var
        self = this,
        uris = [],
        currentUri = {},
        key;
    Parser.prototype.init.call(this);

    this.lineStream = new LineStream();
    this.parseStream = new ParseStream();
    this.lineStream.pipe(this.parseStream);

    // the manifest is empty until the parse stream begins delivering data
    this.manifest = {
        allowCache: true
    };

    // update the manifest with the m3u8 entry from the parse stream
    this.parseStream.on('data', function(entry) {
        ({
            tag: function() {
                // switch based on the tag type
                (({
                    'allow-cache': function() {
                        this.manifest.allowCache = entry.allowed;
                        if (!('allowed' in entry)) {
                            this.trigger('info', {
                                message: 'defaulting allowCache to YES'
                            });
                            this.manifest.allowCache = true;
                        }
                    },
                    'byterange': function() {
                        var byterange = {};
                        if ('length' in entry) {
                            currentUri.byterange = byterange;
                            byterange.length = entry.length;

                            if (!('offset' in entry)) {
                                this.trigger('info', {
                                    message: 'defaulting offset to zero'
                                });
                                entry.offset = 0;
                            }
                        }
                        if ('offset' in entry) {
                            currentUri.byterange = byterange;
                            byterange.offset = entry.offset;
                        }
                    },
                    'endlist': function() {
                        this.manifest.endList = true;
                    },
                    'inf': function() {
                        if (!('mediaSequence' in this.manifest)) {
                            this.manifest.mediaSequence = 0;
                            this.trigger('info', {
                                message: 'defaulting media sequence to zero'
                            });
                        }
                        if (entry.duration >= 0) {
                            currentUri.duration = entry.duration;
                        }

                        this.manifest.segments = uris;

                    },
                    'key': function() {
                        if (!entry.attributes) {
                            this.trigger('warn', {
                                message: 'ignoring key declaration without attribute list'
                            });
                            return;
                        }
                        // clear the active encryption key
                        if (entry.attributes.METHOD === 'NONE') {
                            key = null;
                            return;
                        }
                        if (!entry.attributes.URI) {
                            this.trigger('warn', {
                                message: 'ignoring key declaration without URI'
                            });
                            return;
                        }
                        if (!entry.attributes.METHOD) {
                            this.trigger('warn', {
                                message: 'defaulting key method to AES-128'
                            });
                        }

                        // setup an encryption key for upcoming segments
                        key = {
                            method: entry.attributes.METHOD || 'AES-128',
                            uri: entry.attributes.URI
                        };
                    },
                    'media-sequence': function() {
                        if (!isFinite(entry.number)) {
                            this.trigger('warn', {
                                message: 'ignoring invalid media sequence: ' + entry.number
                            });
                            return;
                        }
                        this.manifest.mediaSequence = entry.number;
                    },
                    'playlist-type': function() {
                        if (!(/VOD|EVENT/).test(entry.playlistType)) {
                            this.trigger('warn', {
                                message: 'ignoring unknown playlist type: ' + entry.playlist
                            });
                            return;
                        }
                        this.manifest.playlistType = entry.playlistType;
                    },
                    'stream-inf': function() {
                        this.manifest.playlists = uris;

                        if (!entry.attributes) {
                            this.trigger('warn', {
                                message: 'ignoring empty stream-inf attributes'
                            });
                            return;
                        }

                        if (!currentUri.attributes) {
                            currentUri.attributes = {};
                        }
                        currentUri.attributes = util.mergeOptions(currentUri.attributes,
                            entry.attributes);
                    },
                    'discontinuity': function() {
                        currentUri.discontinuity = true;
                    },
                    'targetduration': function() {
                        if (!isFinite(entry.duration) || entry.duration < 0) {
                            this.trigger('warn', {
                                message: 'ignoring invalid target duration: ' + entry.duration
                            });
                            return;
                        }
                        this.manifest.targetDuration = entry.duration;
                    },
                    'totalduration': function() {
                        if (!isFinite(entry.duration) || entry.duration < 0) {
                            this.trigger('warn', {
                                message: 'ignoring invalid total duration: ' + entry.duration
                            });
                            return;
                        }
                        this.manifest.totalDuration = entry.duration;
                    }
                })[entry.tagType] || m3util.noop).call(self);
            },
            uri: function() {
                currentUri.uri = entry.uri;
                uris.push(currentUri);

                // if no explicit duration was declared, use the target duration
                if (this.manifest.targetDuration &&
                    !('duration' in currentUri)) {
                    this.trigger('warn', {
                        message: 'defaulting segment duration to the target duration'
                    });
                    currentUri.duration = this.manifest.targetDuration;
                }
                // annotate with encryption information, if necessary
                if (key) {
                    currentUri.key = key;
                }

                // prepare for the next URI
                currentUri = {};
            },
            comment: function() {
                // comments are not important for playback
            }
        })[entry.type].call(self);
    });
};
Parser.prototype = new Stream();
/**
 * Parse the input string and update the manifest object.
 * @param chunk {string} a potentially incomplete portion of the manifest
 */
Parser.prototype.push = function(chunk) {
    this.lineStream.push(chunk);
};
/**
 * Flush any remaining input. This can be handy if the last line of an M3U8
 * manifest did not contain a trailing newline but the file has been
 * completely received.
 */
Parser.prototype.end = function() {
    // flush any buffered input
    this.lineStream.push('\n');
};

module.exports = Parser;