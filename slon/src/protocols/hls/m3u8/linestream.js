var Stream = require('slon/protocols/hls/stream');
/**
 * A stream that buffers string input and generates a `data` event for each
 * line.
 */
LineStream = function() {
    var buffer = '';
    LineStream.prototype.init.call(this);

    /**
     * Add new data to be parsed.
     * @param data {string} the text to process
     */
    this.push = function(data) {
        var nextNewline;

        buffer += data;
        nextNewline = buffer.indexOf('\n');

        for (; nextNewline > -1; nextNewline = buffer.indexOf('\n')) {
            this.trigger('data', buffer.substring(0, nextNewline));
            buffer = buffer.substring(nextNewline + 1);
        }
    };
};
LineStream.prototype = new Stream();

module.exports = LineStream;