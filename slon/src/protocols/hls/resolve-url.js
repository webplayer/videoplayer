module.exports = function(basePath, path) {
    // use the base element to get the browser to handle URI resolution
    var
        oldBase = document.querySelector('base'),
        docHead = document.querySelector('head'),
        a = document.createElement('a'),
        base = oldBase,
        oldHref,
        result;

    // prep the document
    if (oldBase) {
        oldHref = oldBase.href;
    } else {
        base = docHead.appendChild(document.createElement('base'));
    }

    base.href = basePath;
    a.href = path;
    result = a.href;

    // clean up
    if (oldBase) {
        oldBase.href = oldHref;
    } else {
        docHead.removeChild(base);
    }
    return result;
};