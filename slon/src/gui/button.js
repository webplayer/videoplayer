var Component = require('slon/component');
var OBJ = require('slon/lib/obj');
var DOM = require('slon/lib/dom');
var events = require('slon/lib/events');

/* Button - Base class for all buttons
================================================================================ */
/**
 * Base class for all buttons
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */
 var Button = Component.extend({
  /**
   * @constructor
   * @inheritDoc
   */
  init: function(player, options){
    Component.call(this, player, options);

    this.emitTapEvents();

    this.on('tap', this.onClick);
    this.on('click', this.onClick);
    this.on('focus', this.onFocus);
    this.on('blur', this.onBlur);
  }
});

Button.prototype.createEl = function(type, props){
  var el;

  // Add standard Aria and Tabindex info
  props = OBJ.merge({
    className: this.buildCSSClass(),
    'role': 'button',
    'aria-live': 'polite', // let the screen reader user know that the text of the button may change
    tabIndex: 0
  }, props);

  el = Component.prototype.createEl.call(this, type, props);

  // if innerHTML hasn't been overridden (bigPlayButton), add content elements
  if (!props.innerHTML) {
    this.contentEl_ = DOM.createEl('div', {
      className: 'slon-control-content'
    });

    this.controlText_ = DOM.createEl('span', {
      className: 'slon-control-text',
      innerHTML: this.localize(this.buttonText) || 'Need Text'
    });

    this.contentEl_.appendChild(this.controlText_);
    el.appendChild(this.contentEl_);
  }

  return el;
};

Button.prototype.buildCSSClass = function(){
  // TODO: Change slon-control to slon-button?
  return 'slon-control ' + Component.prototype.buildCSSClass.call(this);
};

  // Click - Override with specific functionality for button
Button.prototype.onClick = function(){};

  // Focus - Add keyboard functionality to element
Button.prototype.onFocus = function(){
  events.on(document, 'keyup', events.bind(this, this.onKeyPress));
};

  // KeyPress (document level) - Trigger click when keys are pressed
Button.prototype.onKeyPress = function(event){
  // Check for space bar (32) or enter (13) keys
  if (event.which == 32 || event.which == 13) {
    event.preventDefault();
    this.onClick();
  }
};

// Blur - Remove keyboard triggers
Button.prototype.onBlur = function(){
  events.off(document, 'keyup', events.bind(this, this.onKeyPress));
};


// Component.components['Button'] = Button;
module.exports = Button;