var Button = require('slon/gui/button');

var OBJ = require('slon/lib/obj');


/**
 * The component for a menu item. `<li>`
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */
var MenuItem = Button.extend({
  /** @constructor */
  init: function(player, options){
    Button.call(this, player, options);
    this.selected(options['selected']);
  }
});

/** @inheritDoc */
MenuItem.prototype.createEl = function(type, props){
  return Button.prototype.createEl.call(this, 'li', OBJ.merge({
    className: 'slon-menu-item',
    innerHTML: this.options_['label']
  }, props));
};

/**
 * Handle a click on the menu item, and set it to selected
 */
MenuItem.prototype.onClick = function(){
  this.selected(true);
};

/**
 * Set this menu item as selected or not
 * @param  {Boolean} selected
 */
MenuItem.prototype.selected = function(selected){
  if (selected) {
    this.addClass('slon-selected');
    this.el_.setAttribute('aria-selected',true);
  } else {
    this.removeClass('slon-selected');
    this.el_.setAttribute('aria-selected',false);
  }
};

module.exports = MenuItem;