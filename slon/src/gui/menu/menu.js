var Component = require('slon/component');

var DOM = require('slon/lib/dom');
var OBJ = require('slon/lib/obj');

var util = require('slon/lib/util');
var events = require('slon/lib/events');

/* Menu
================================================================================ */
/**
 * The Menu component is used to build pop up menus, including subtitle and
 * captions selection menus.
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */
var Menu = Component.extend();

/**
 * Add a menu item to the menu
 * @param {Object|String} component Component or component type to add
 */
Menu.prototype.addItem = function(component){
  this.addChild(component);
  component.on('click', events.bind(this, function(){
    this.unlockShowing();
  }));
};

/** @inheritDoc */
Menu.prototype.createEl = function(){
  var contentElType = this.options().contentElType || 'ul';
  this.contentEl_ = DOM.createEl(contentElType, {
    className: 'slon-menu-content'
  });
  var el = Component.prototype.createEl.call(this, 'div', {
    append: this.contentEl_,
    className: 'slon-menu'
  });
  el.appendChild(this.contentEl_);

  // Prevent clicks from bubbling up. Needed for Menu Buttons,
  // where a click on the parent is significant
  events.on(el, 'click', function(event){
    event.preventDefault();
    event.stopImmediatePropagation();
  });

  return el;
};

module.exports = Menu;