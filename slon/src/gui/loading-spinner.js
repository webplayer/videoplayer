/* Loading Spinner
================================================================================ */
/**
 * Loading spinner for waiting events
 * @param {slon.Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */

var Component = require('slon/component');

var LoadingSpinner = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);

    // MOVING DISPLAY HANDLING TO CSS

    // player.on('canplay', events.bind(this, this.hide));
    // player.on('canplaythrough', events.bind(this, this.hide));
    // player.on('playing', events.bind(this, this.hide));
    // player.on('seeking', events.bind(this, this.show));

    // in some browsers seeking does not trigger the 'playing' event,
    // so we also need to trap 'seeked' if we are going to set a
    // 'seeking' event
    // player.on('seeked', events.bind(this, this.hide));

    // player.on('ended', events.bind(this, this.hide));

    // Not showing spinner on stalled any more. Browsers may stall and then not trigger any events that would remove the spinner.
    // Checked in Chrome 16 and Safari 5.1.2. http://help.slon.com/discussions/problems/883-why-is-the-download-progress-showing
    // player.on('stalled', events.bind(this, this.show));

    // player.on('waiting', events.bind(this, this.show));
  }
});

LoadingSpinner.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-loading-spinner'
  });
};

module.exports = LoadingSpinner;