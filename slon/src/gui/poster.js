var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var events = require('slon/lib/events');

var Component = require('slon/component');
var Button = require('slon/gui/button');

/* Poster Image
================================================================================ */
/**
 * The component that handles showing the poster image.
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var PosterImage = Button.extend({
  /** @constructor */
  init: function(player, options){
    Button.call(this, player, options);

    if (player.poster()) {
      this.src(player.poster());
    }

    if (!player.poster() || !player.controls()) {
      this.hide();
    }

    player.on('posterchange', events.bind(this, function(){
      this.src(player.poster());
    }));

    if (!player.isAudio()) {
      player.on('play', events.bind(this, this.hide));
    }

    player.on('play', events.bind(this, this.hide));
  }
});

// use the test el to check for backgroundSize style support
var _backgroundSizeSupported = 'backgroundSize' in cache.TEST_VID.style;

PosterImage.prototype.createEl = function(){
  var el = DOM.createEl('div', {
    className: 'slon-poster',

    // Don't want poster to be tabbable.
    tabIndex: -1
  });

  if (!_backgroundSizeSupported) {
    // setup an img element as a fallback for IE8
    el.appendChild(DOM.createEl('img'));
  }

  return el;
};

PosterImage.prototype.src = function(url){
  var el = this.el();

  // getter
  // can't think of a need for a getter here
  // see #838 if on is needed in the future
  // still don't want a getter to set src as undefined
  if (url === undefined) {
    return;
  }

  // setter
  // To ensure the poster image resizes while maintaining its original aspect
  // ratio, use a div with `background-size` when available. For browsers that
  // do not support `background-size` (e.g. IE8), fall back on using a regular
  // img element.
  if (_backgroundSizeSupported) {
    el.style.backgroundImage = 'url("' + url + '")';
  } else {
    el.firstChild.src = url;
  }
};

PosterImage.prototype.onClick = function(){
  // Only accept clicks when controls are enabled
  if (this.player().controls()) {
    this.player_.play();
  }
};

Component.components['PosterImage'] = PosterImage;
module.exports = PosterImage;