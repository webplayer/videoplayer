var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var util = require('slon/lib/util');
var events = require('slon/lib/events');

var Component = require('slon/component');

/**
 * The separator between the current time and duration
 *
 * Can be hidden if it's not needed in the design.
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
TimeDivider = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);
  }
});

TimeDivider.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-time-divider',
    innerHTML: '<div><span>/</span></div>'
  });
};

module.exports = TimeDivider;