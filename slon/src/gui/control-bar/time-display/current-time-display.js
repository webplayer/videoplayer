var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var util = require('slon/lib/util');
var events = require('slon/lib/events');

var Component = require('slon/component');


/**
 * Displays the current time
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var CurrentTimeDisplay = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);

    player.on('timeupdate', events.bind(this, this.updateContent));
  }
});

CurrentTimeDisplay.prototype.createEl = function(){
  var el = Component.prototype.createEl.call(this, 'div', {
    className: 'slon-current-time slon-time-controls slon-control'
  });

  this.contentEl_ = DOM.createEl('div', {
    className: 'slon-current-time-display',
    innerHTML: '<span class="slon-control-text">Current Time </span>' + '0:00', // label the current time for screen reader users
    'aria-live': 'off' // tell screen readers not to automatically read the time as it changes
  });

  el.appendChild(this.contentEl_);
  return el;
};

CurrentTimeDisplay.prototype.updateContent = function(){
  // Allows for smooth scrubbing, when player can't keep up.
  var time = (this.player_.scrubbing) ? this.player_.getCache().currentTime : this.player_.currentTime();
  this.contentEl_.innerHTML = '<span class="slon-control-text">' + this.localize('Current Time') + '</span> ' + util.formatTime(time, this.player_.duration());
};

module.exports = CurrentTimeDisplay;