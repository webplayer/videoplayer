var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var util = require('slon/lib/util');
var events = require('slon/lib/events');

var Component = require('slon/component');

/**
 * Displays the duration
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var DurationDisplay = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);

    // this might need to be changed to 'durationchange' instead of 'timeupdate' eventually,
    // however the durationchange event fires before this.player_.duration() is set,
    // so the value cannot be written out using this method.
    // Once the order of durationchange and this.player_.duration() being set is figured out,
    // this can be updated.
    player.on('timeupdate', events.bind(this, this.updateContent));
  }
});

DurationDisplay.prototype.createEl = function(){
  var el = Component.prototype.createEl.call(this, 'div', {
    className: 'slon-duration slon-time-controls slon-control'
  });

  this.contentEl_ = DOM.createEl('div', {
    className: 'slon-duration-display',
    innerHTML: '<span class="slon-control-text">' + this.localize('Duration Time') + '</span> ' + '0:00', // label the duration time for screen reader users
    'aria-live': 'off' // tell screen readers not to automatically read the time as it changes
  });

  el.appendChild(this.contentEl_);
  return el;
};

DurationDisplay.prototype.updateContent = function(){
  var duration = this.player_.duration();
  if (duration) {
      this.contentEl_.innerHTML = '<span class="slon-control-text">' + this.localize('Duration Time') + '</span> ' + util.formatTime(duration); // label the duration time for screen reader users
  }
};

module.exports = DurationDisplay;