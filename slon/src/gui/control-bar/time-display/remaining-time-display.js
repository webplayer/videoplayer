var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var util = require('slon/lib/util');
var events = require('slon/lib/events');

var Component = require('slon/component');

/**
 * Displays the time left in the video
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var RemainingTimeDisplay = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);

    player.on('timeupdate', events.bind(this, this.updateContent));
  }
});

RemainingTimeDisplay.prototype.createEl = function(){
  var el = Component.prototype.createEl.call(this, 'div', {
    className: 'slon-remaining-time slon-time-controls slon-control'
  });

  this.contentEl_ = DOM.createEl('div', {
    className: 'slon-remaining-time-display',
    innerHTML: '<span class="slon-control-text">' + this.localize('Remaining Time') + '</span> ' + '-0:00', // label the remaining time for screen reader users
    'aria-live': 'off' // tell screen readers not to automatically read the time as it changes
  });

  el.appendChild(this.contentEl_);
  return el;
};

RemainingTimeDisplay.prototype.updateContent = function(){
  if (this.player_.duration()) {
    this.contentEl_.innerHTML = '<span class="slon-control-text">' + this.localize('Remaining Time') + '</span> ' + '-'+ util.formatTime(this.player_.remainingTime());
  }

  // Allows for smooth scrubbing, when player can't keep up.
  // var time = (this.player_.scrubbing) ? this.player_.getCache().currentTime : this.player_.currentTime();
  // this.contentEl_.innerHTML = util.formatTime(time, this.player_.duration());
};

module.exports = RemainingTimeDisplay;