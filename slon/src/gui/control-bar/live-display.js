var DOM = require('slon/lib/dom');
var Component = require('slon/component');

/**
 * Displays the live indicator
 * TODO - Future make it click to snap to live
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var LiveDisplay = Component.extend({
    init: function(player, options) {
        Component.call(this, player, options);
    }
});

LiveDisplay.prototype.createEl = function() {
    var el = Component.prototype.createEl.call(this, 'div', {
        className: 'slon-live-controls slon-control'
    });

    this.contentEl_ = DOM.createEl('div', {
        className: 'slon-live-display',
        innerHTML: '<span class="slon-control-text">' + this.localize('Stream Type') + '</span>' + this.localize('LIVE'),
        'aria-live': 'off'
    });

    el.appendChild(this.contentEl_);

    return el;
};

module.exports = LiveDisplay;
