var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Button = require('slon/gui/button');

var MenuButton = require('slon/gui/menu/button');
var Menu = require('slon/gui/menu/menu');

var MuteToggle = require('slon/gui/control-bar/mute-toggle');

var VolumeBar = require('slon/gui/control-bar/volume/bar');


/**
 * Menu button with a popup for showing the volume slider.
 * @constructor
 */
var VolumeMenuButton = MenuButton.extend({
  /** @constructor */
  init: function(player, options){
    MenuButton.call(this, player, options);

    // Same listeners as MuteToggle
    player.on('volumechange', events.bind(this, this.update));

    // hide mute toggle if the current tech doesn't support volume control
    if (player.tech && player.tech.features && player.tech.features.volumeControl === false) {
      this.addClass('slon-hidden');
    }
    player.on('loadstart', events.bind(this, function(){
      if (player.tech.features && player.tech.features.volumeControl === false) {
        this.addClass('slon-hidden');
      } else {
        this.removeClass('slon-hidden');
      }
    }));
    this.addClass('slon-menu-button');
  }
});

VolumeMenuButton.prototype.createMenu = function(){
  var menu = new Menu(this.player_, {
    contentElType: 'div'
  });
  var vc = new VolumeBar(this.player_, OBJ.merge({'vertical': true}, this.options_.volumeBar));
  menu.addChild(vc);
  return menu;
};

VolumeMenuButton.prototype.onClick = function(){
  MuteToggle.prototype.onClick.call(this);
  MenuButton.prototype.onClick.call(this);
};

VolumeMenuButton.prototype.createEl = function(){
  return Button.prototype.createEl.call(this, 'div', {
    className: 'slon-volume-menu-button slon-menu-button slon-control',
    innerHTML: '<div><span class="slon-control-text">' + this.localize('Mute') + '</span></div>'
  });
};
VolumeMenuButton.prototype.update = MuteToggle.prototype.update;

module.exports = VolumeMenuButton;