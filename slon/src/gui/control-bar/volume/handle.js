var SliderHandle = require('slon/gui/slider/handle');

/**
 * The volume handle can be dragged to adjust the volume level
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
 var VolumeHandle = SliderHandle.extend();

 VolumeHandle.prototype.defaultValue = '00:00';

 /** @inheritDoc */
 VolumeHandle.prototype.createEl = function(){
   return SliderHandle.prototype.createEl.call(this, 'div', {
     className: 'slon-volume-handle'
   });
 };

module.exports = VolumeHandle;