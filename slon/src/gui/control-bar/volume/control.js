var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');

/**
 * The component for controlling the volume level
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var VolumeControl = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);

    // hide volume controls when they're not supported by the current tech
    if (player.tech && player.tech.features && player.tech.features['volumeControl'] === false) {
      this.addClass('slon-hidden');
    }
    player.on('loadstart', events.bind(this, function(){
      if (player.tech.features && player.tech.features['volumeControl'] === false) {
        this.addClass('slon-hidden');
      } else {
        this.removeClass('slon-hidden');
      }
    }));
  }
});

VolumeControl.prototype.options_ = {
  children: {
    'volumeBar': {}
  }
};

VolumeControl.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-volume-control slon-control'
  });
};

module.exports = VolumeControl;