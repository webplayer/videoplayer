var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');
var Slider = require('slon/gui/slider/slider');

/**
 * The bar that contains the volume level and can be clicked on to adjust the level
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var VolumeBar = Slider.extend({
  /** @constructor */
  init: function(player, options){
    Slider.call(this, player, options);
    player.on('volumechange', events.bind(this, this.updateARIAAttributes));
    player.ready(events.bind(this, this.updateARIAAttributes));
  }
});

VolumeBar.prototype.updateARIAAttributes = function(){
  // Current value of volume bar as a percentage
  this.el_.setAttribute('aria-valuenow',util.round(this.player_.volume()*100, 2));
  this.el_.setAttribute('aria-valuetext',util.round(this.player_.volume()*100, 2)+'%');
};

VolumeBar.prototype.options_ = {
  children: {
    'volumeLevel': {},
    'volumeHandle': {}
  },
  'barName': 'volumeLevel',
  'handleName': 'volumeHandle'
};

VolumeBar.prototype.playerEvent = 'volumechange';

VolumeBar.prototype.createEl = function(){
  return Slider.prototype.createEl.call(this, 'div', {
    className: 'slon-volume-bar',
    'aria-label': 'volume level'
  });
};

VolumeBar.prototype.onMouseMove = function(event) {
  if (this.player_.muted()) {
    this.player_.muted(false);
  }

  this.player_.volume(this.calculateDistance(event));
};

VolumeBar.prototype.getPercent = function(){
  if (this.player_.muted()) {
    return 0;
  } else {
    return this.player_.volume();
  }
};

VolumeBar.prototype.stepForward = function(){
  this.player_.volume(this.player_.volume() + 0.1);
};

VolumeBar.prototype.stepBack = function(){
  this.player_.volume(this.player_.volume() - 0.1);
};

module.exports = VolumeBar;