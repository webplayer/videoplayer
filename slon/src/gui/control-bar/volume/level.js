var Component = require('slon/component');

/**
 * Shows volume level
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var VolumeLevel = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);
  }
});

VolumeLevel.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-volume-level',
    innerHTML: '<span class="slon-control-text"></span>'
  });
};

module.exports = VolumeLevel;