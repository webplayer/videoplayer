var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');

/**
 * The Progress Control component contains the seek bar, load progress,
 * and play progress
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var ProgressControl = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);
  }
});

ProgressControl.prototype.options_ = {
  children: {
    'seekBar': {}
  }
};

ProgressControl.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-progress-control slon-control'
  });
};

module.exports = ProgressControl;