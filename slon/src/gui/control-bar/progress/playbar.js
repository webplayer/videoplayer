var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');

/**
 * Shows play progress
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
PlayProgressBar = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);
  }
});

PlayProgressBar.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-play-progress',
    innerHTML: '<span class="slon-control-text"><span>' + this.localize('Progress') + '</span>: 0%</span>'
  });
};

module.exports = PlayProgressBar;