var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');
var SliderHandle = require('slon/gui/slider/handle');

/**
 * The Seek Handle shows the current position of the playhead during playback,
 * and can be dragged to adjust the playhead.
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var SeekHandle = SliderHandle.extend({
  init: function(player, options) {
    SliderHandle.call(this, player, options);
    player.on('timeupdate', events.bind(this, this.updateContent));
  }
});

/**
 * The default value for the handle content, which may be read by screen readers
 *
 * @type {String}
 * @private
 */
SeekHandle.prototype.defaultValue = '00:00';

/** @inheritDoc */
SeekHandle.prototype.createEl = function() {
  return SliderHandle.prototype.createEl.call(this, 'div', {
    className: 'slon-seek-handle',
    'aria-live': 'off'
  });
};

SeekHandle.prototype.updateContent = function() {
  var time = (this.player_.scrubbing) ? this.player_.getCache().currentTime : this.player_.currentTime();
  this.el_.innerHTML = '<span class="slon-control-text">' + util.formatTime(time, this.player_.duration()) + '</span>';
};

module.exports = SeekHandle;