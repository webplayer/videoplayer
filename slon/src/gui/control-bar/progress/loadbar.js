var OBJ = require('slon/lib/obj');
var DOM = require('slon/lib/dom');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');

/**
 * Shows load progress
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var LoadProgressBar = Component.extend({
  /** @constructor */
  init: function(player, options){
    Component.call(this, player, options);
    player.on('progress', events.bind(this, this.update));
  }
});

LoadProgressBar.prototype.createEl = function(){
  return Component.prototype.createEl.call(this, 'div', {
    className: 'slon-load-progress',
    innerHTML: '<span class="slon-control-text"><span>' + this.localize('Loaded') + '</span>: 0%</span>'
  });
};

LoadProgressBar.prototype.update = function(){
  var i, start, end, part,
      buffered = this.player_.buffered(),
      duration = this.player_.duration(),
      bufferedEnd = this.player_.bufferedEnd(),
      children = this.el_.children,
      // get the percent width of a time compared to the total end
      percentify = function (time, end){
        var percent = (time / end) || 0; // no NaN
        return (percent * 100) + '%';
      };

  // update the width of the progress bar
  this.el_.style.width = percentify(bufferedEnd, duration);

  // add child elements to represent the individual buffered time ranges
  for (i = 0; i < buffered.length; i++) {
    start = buffered.start(i),
    end = buffered.end(i),
    part = children[i];

    if (!part) {
      part = this.el_.appendChild(DOM.createEl())
    };

    // set the percent based on the width of the progress bar (bufferedEnd)
    part.style.left = percentify(start, bufferedEnd);
    part.style.width = percentify(end - start, bufferedEnd);
  };

  // remove unused buffered range elements
  for (i = children.length; i > buffered.length; i--) {
    this.el_.removeChild(children[i-1]);
  }
};

module.exports = LoadProgressBar;