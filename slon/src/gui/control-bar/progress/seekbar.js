var OBJ = require('slon/lib/obj');

var events = require('slon/lib/events');
var util = require('slon/lib/util');

var Component = require('slon/component');
var Slider = require('slon/gui/slider/slider');

/**
 * Seek Bar and holder for the progress bars
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var SeekBar = Slider.extend({
  /** @constructor */
  init: function(player, options){
    Slider.call(this, player, options);
    player.on('timeupdate', events.bind(this, this.updateARIAAttributes));
    player.ready(events.bind(this, this.updateARIAAttributes));
  }
});

SeekBar.prototype.options_ = {
  children: {
    'loadProgressBar': {},
    'playProgressBar': {},
    'seekHandle': {}
  },
  'barName': 'playProgressBar',
  'handleName': 'seekHandle'
};

SeekBar.prototype.playerEvent = 'timeupdate';

SeekBar.prototype.createEl = function(){
  return Slider.prototype.createEl.call(this, 'div', {
    className: 'slon-progress-holder',
    'aria-label': 'video progress bar'
  });
};

SeekBar.prototype.updateARIAAttributes = function(){
    // Allows for smooth scrubbing, when player can't keep up.
    var time = (this.player_.scrubbing) ? this.player_.getCache().currentTime : this.player_.currentTime();
    this.el_.setAttribute('aria-valuenow',util.round(this.getPercent()*100, 2)); // machine readable value of progress bar (percentage complete)
    this.el_.setAttribute('aria-valuetext',util.formatTime(time, this.player_.duration())); // human readable value of progress bar (time complete)
};

SeekBar.prototype.getPercent = function(){
  return this.player_.currentTime() / this.player_.duration();
};

SeekBar.prototype.onMouseDown = function(event){
  Slider.prototype.onMouseDown.call(this, event);

  this.player_.scrubbing = true;

  this.videoWasPlaying = !this.player_.paused();
  this.player_.pause();
};

SeekBar.prototype.onMouseMove = function(event){
  var newTime = this.calculateDistance(event) * this.player_.duration();

  // Don't let video end while scrubbing.
  if (newTime == this.player_.duration()) { newTime = newTime - 0.1; }

  // Set new time (tell player to seek to new time)
  this.player_.currentTime(newTime);
};

SeekBar.prototype.onMouseUp = function(event){
  Slider.prototype.onMouseUp.call(this, event);

  this.player_.scrubbing = false;
  if (this.videoWasPlaying) {
    this.player_.play();
  }
};

SeekBar.prototype.stepForward = function(){
  this.player_.currentTime(this.player_.currentTime() + 5); // more quickly fast forward for keyboard-only users
};

SeekBar.prototype.stepBack = function(){
  this.player_.currentTime(this.player_.currentTime() - 5); // more quickly rewind for keyboard-only users
};

module.exports = SeekBar;