var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var events = require('slon/lib/events');

var Component = require('slon/component');
var MenuButton = require('slon/gui/menu/button');
var MenuItem = require('slon/gui/menu/item');

/**
 * The specific menu item type for selecting a playback rate
 *
 * @constructor
 */
var PlaybackRateMenuItem = MenuItem.extend({
  contentElType: 'button',
  /** @constructor */
  init: function(player, options){
    var label = this.label = options['rate'];
    var rate = this.rate = parseFloat(label, 10);

    // Modify options for parent MenuItem class's init.
    options['label'] = label;
    options['selected'] = rate === 1;
    MenuItem.call(this, player, options);

    this.player().on('ratechange', events.bind(this, this.update));
  }
});

PlaybackRateMenuItem.prototype.onClick = function(){
  MenuItem.prototype.onClick.call(this);
  this.player().playbackRate(this.rate);
};

PlaybackRateMenuItem.prototype.update = function(){
  this.selected(this.player().playbackRate() == this.rate);
};

module.exports = PlaybackRateMenuItem;