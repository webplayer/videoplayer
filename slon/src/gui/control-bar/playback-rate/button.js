var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var events = require('slon/lib/events');

var Component = require('slon/component');

var Menu = require('slon/gui/menu/menu');
var MenuButton = require('slon/gui/menu/button');
var MenuItem = require('slon/gui/menu/item');

var PlaybackRateMenuItem = require('slon/gui/control-bar/playback-rate/item');

/**
 * The component for controlling the playback rate
 *
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var PlaybackRateMenuButton = MenuButton.extend({
  /** @constructor */
  init: function(player, options){
    MenuButton.call(this, player, options);

    this.updateVisibility();
    this.updateLabel();

    player.on('loadstart', events.bind(this, this.updateVisibility));
    player.on('ratechange', events.bind(this, this.updateLabel));
  }
});

PlaybackRateMenuButton.prototype.createEl = function(){
  var el = Component.prototype.createEl.call(this, 'div', {
    className: 'slon-playback-rate slon-menu-button slon-control',
    innerHTML: '<div class="slon-control-content"><span class="slon-control-text">' + this.localize('Playback Rate') + '</span></div>'
  });

  this.labelEl_ = DOM.createEl('div', {
    className: 'slon-playback-rate-value',
    innerHTML: 1.0
  });

  el.appendChild(this.labelEl_);

  return el;
};

// Menu creation
PlaybackRateMenuButton.prototype.createMenu = function(){
  var menu = new Menu(this.player());
  var rates = this.player().options()['playbackRates'];

  if (rates) {
    for (var i = rates.length - 1; i >= 0; i--) {
      menu.addChild(
        new PlaybackRateMenuItem(this.player(), { 'rate': rates[i] + 'x'})
        );
    };
  }

  return menu;
};

PlaybackRateMenuButton.prototype.updateARIAAttributes = function(){
  // Current playback rate
  this.el().setAttribute('aria-valuenow', this.player().playbackRate());
};

PlaybackRateMenuButton.prototype.onClick = function(){
  // select next rate option
  var currentRate = this.player().playbackRate();
  var rates = this.player().options()['playbackRates'];
  // this will select first one if the last one currently selected
  var newRate = rates[0];
  for (var i = 0; i <rates.length ; i++) {
    if (rates[i] > currentRate) {
      newRate = rates[i];
      break;
    }
  };
  this.player().playbackRate(newRate);
};

PlaybackRateMenuButton.prototype.playbackRateSupported = function(){
  return this.player().tech
    && this.player().tech.features['playbackRate']
    && this.player().options()['playbackRates']
    && this.player().options()['playbackRates'].length > 0
  ;
};

/**
 * Hide playback rate controls when they're no playback rate options to select
 */
PlaybackRateMenuButton.prototype.updateVisibility = function(){
  if (this.playbackRateSupported()) {
    this.removeClass('slon-hidden');
  } else {
    this.addClass('slon-hidden');
  }
};

/**
 * Update button label when rate changed
 */
PlaybackRateMenuButton.prototype.updateLabel = function(){
  if (this.playbackRateSupported()) {
    this.labelEl_.innerHTML = this.player().playbackRate() + 'x';
  }
};

module.exports = PlaybackRateMenuButton; 