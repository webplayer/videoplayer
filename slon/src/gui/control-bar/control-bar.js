var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var events = require('slon/lib/events');

var Component = require('slon/component');


/**
 * Container of main controls
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 * @extends Component
 */
var ControlBar = Component.extend();

ControlBar.prototype.options_ = {
  loadEvent: 'play',
  children: {
    'playToggle': {},
    
    'progressControl': {},
    
    'currentTimeDisplay': {},
    'timeDivider': {},
    'durationDisplay': {},
    'remainingTimeDisplay': {},
    'liveDisplay': {},
    
    'fullscreenToggle': {},

    'volumeControl': {},
    'muteToggle': {},
    // 'volumeMenuButton': {},
    // 'playbackRateMenuButton': {}

  }
};

ControlBar.prototype.createEl = function(){
  return DOM.createEl('div', {
    className: 'slon-control-bar'
  });
};

module.exports = ControlBar;