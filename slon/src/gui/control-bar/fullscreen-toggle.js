var Button = require('slon/gui/button');

/**
 * Toggle fullscreen video
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @extends Button
 */
var FullscreenToggle = Button.extend({
  /**
   * @constructor
   * @memberof FullscreenToggle
   * @instance
   */
  init: function(player, options){
    Button.call(this, player, options);
  }
});

FullscreenToggle.prototype.buttonText = 'Fullscreen';

FullscreenToggle.prototype.buildCSSClass = function(){
  return 'slon-fullscreen-control ' + Button.prototype.buildCSSClass.call(this);
};

FullscreenToggle.prototype.onClick = function(){
  if (!this.player_.isFullscreen()) {
    this.player_.requestFullscreen();
    this.controlText_.innerHTML = this.localize('Non-Fullscreen');
  } else {
    this.player_.exitFullscreen();
    this.controlText_.innerHTML = this.localize('Fullscreen');
  }
};

module.exports = FullscreenToggle;