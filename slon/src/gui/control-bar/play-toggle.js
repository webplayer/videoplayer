var Button = require('slon/gui/button');
var DOM = require('slon/lib/dom');
var events = require('slon/lib/events');

/**
 * Button to toggle between play and pause
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */
var PlayToggle = Button.extend({
  /** @constructor */
  init: function(player, options){
    Button.call(this, player, options);

    player.on('play', events.bind(this, this.onPlay));
    player.on('pause', events.bind(this, this.onPause));
  }
});

PlayToggle.prototype.buttonText = 'Play';

PlayToggle.prototype.buildCSSClass = function(){
  return 'slon-play-control ' + Button.prototype.buildCSSClass.call(this);
};

// OnClick - Toggle between play and pause
PlayToggle.prototype.onClick = function(){
  if (this.player_.paused()) {
    this.player_.play();
  } else {
    this.player_.pause();
  }
};

// OnPlay - Add the slon-playing class to the element so it can change appearance
PlayToggle.prototype.onPlay = function(){
  DOM.removeClass(this.el_, 'slon-paused');
  DOM.addClass(this.el_, 'slon-playing');
  this.el_.children[0].children[0].innerHTML = this.localize('Pause'); // change the button text to "Pause"
};

// OnPause - Add the slon-paused class to the element so it can change appearance
PlayToggle.prototype.onPause = function(){
  DOM.removeClass(this.el_, 'slon-playing');
  DOM.addClass(this.el_, 'slon-paused');
  this.el_.children[0].children[0].innerHTML = this.localize('Play'); // change the button text to "Play"
};

module.exports = PlayToggle;