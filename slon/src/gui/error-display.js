var Component = require('slon/component');

var events = require('slon/lib/events');
var DOM = require('slon/lib/dom');


/**
 * Display that an error has occurred making the video unplayable
 * @param {Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var ErrorDisplay = Component.extend({
  init: function(player, options){
    Component.call(this, player, options);

    this.update();
    player.on('error', events.bind(this, this.update));
  }
});

ErrorDisplay.prototype.createEl = function(){
  var el = Component.prototype.createEl.call(this, 'div', {
    className: 'slon-error-display'
  });

  this.contentEl_ = DOM.createEl('div');
  el.appendChild(this.contentEl_);

  return el;
};

ErrorDisplay.prototype.update = function(){
  if (this.player().error()) {
    this.contentEl_.innerHTML = this.localize(this.player().error().message);
  }
};

module.exports = ErrorDisplay;