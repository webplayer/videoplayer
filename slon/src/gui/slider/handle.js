var OBJ = require('slon/lib/obj');

var Component = require('slon/component');


/**
 * SeekBar Behavior includes play progress bar, and seek handle
 * Needed so it can determine seek position based on handle position/size
 * @param {slon.Player|Object} player
 * @param {Object=} options
 * @constructor
 */
SliderHandle = Component.extend();

/**
 * Default value of the slider
 *
 * @type {Number}
 * @private
 */
SliderHandle.prototype.defaultValue = 0;

/** @inheritDoc */
SliderHandle.prototype.createEl = function(type, props) {
  props = props || {};
  // Add the slider element class to all sub classes
  props.className = props.className + ' slon-slider-handle';
  props = OBJ.merge({
    innerHTML: '<span class="slon-control-text">'+this.defaultValue+'</span>'
  }, props);

  return Component.prototype.createEl.call(this, 'div', props);
};

module.exports = SliderHandle;