var DOM = require('slon/lib/dom');

var cache = require('slon/lib/cache');
var events = require('slon/lib/events');

var Component = require('slon/component');
var Button = require('slon/gui/button');

/* Big Play Button
================================================================================ */
/**
 * Initial play button. Shows before the video has played. The hiding of the
 * big play button is done via CSS and player states.
 * @param {Player|Object} player
 * @param {Object=} options
 * @class
 * @constructor
 */

var BigPlayButton = Button.extend();

BigPlayButton.prototype.createEl = function(){
  return Button.prototype.createEl.call(this, 'div', {
    className: 'slon-big-play-button',
    innerHTML: '<span aria-hidden="true"></span>',
    'aria-label': 'play video'
  });
};

BigPlayButton.prototype.onClick = function(){
  // this.player_.play();
  if (this.player_.paused()) {
    this.player_.play();
  } else {
    this.player_.pause();
  }
};

module.exports = BigPlayButton