package com.cdnvideo.slon.test
{
	import org.flexunit.Assert;
	import com.cdnvideo.slon.SlonModel;
	
	public class SlonModelTest
	{		
		[Test]
		public function test_backgroundColor():void
		{
			SlonModel.getInstance().backgroundColor = -1;
			Assert.assertEquals(0, SlonModel.getInstance().backgroundColor);
		
			SlonModel.getInstance().backgroundColor = 5;
			Assert.assertEquals(5, SlonModel.getInstance().backgroundColor);
			
			SlonModel.getInstance().backgroundColor = 0;
			Assert.assertEquals(0, SlonModel.getInstance().backgroundColor);		
		}
		
		[Test]
		public function test_volume():void
		{
			SlonModel.getInstance().volume = -1;
			Assert.assertEquals(1, SlonModel.getInstance().volume);

			SlonModel.getInstance().volume = 2;
			Assert.assertEquals(1, SlonModel.getInstance().volume);

			SlonModel.getInstance().volume = 0.5;
			Assert.assertEquals(0.5, SlonModel.getInstance().volume);
		}
	}
}