package com.cdnvideo.slon.events{
    
    import flash.events.Event;
    
    public class SlonEvent extends Event{
        
        public static const STAGE_RESIZE:String = "SlonEvent.STAGE_RESIZE";
        public static const BACKGROUND_COLOR_SET:String = "SlonEvent.BACKGROUND_COLOR_SET";
        public static const POSTER_SET:String = "SlonEvent.POSTER_SET";
        
        // a flexible container object for whatever data needs to be attached to any of these events
        private var _data:Object;
        
        public function SlonEvent(pType:String, pData:Object = null){
            super(pType, true, false);
            _data = pData;
        }
        
        public function get data():Object {
            return _data;
        }
        
    }
}