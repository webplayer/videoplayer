package com.cdnvideo.slon{
    
    import flash.display.Sprite;
	
    public class SlonApp extends Sprite{
        
        private var _uiView:SlonView;
        private var _model:SlonModel;
        
        public function SlonApp(){
            
            _model = SlonModel.getInstance()

            _uiView = new SlonView();
            addChild(_uiView);

        }
        
        public function get model():SlonModel{
            return _model;
        }
        
    }
}