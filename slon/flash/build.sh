../../utils/flex/current/bin/mxmlc \
-title 'SlonPlayer' -description 'CDN player' \
-publisher 'A Grechishkin' -creator 'A Grechishkin' \
-language='EN' -default-size 640 360 -default-frame-rate=30 \
-default-background-color=0x000000 -default-script-limits 1000 60 \
-library-path+=libs/flexunit-4.1.0-8-as3_4.1.0.16076.swc \
-library-path+=libs/flexunit-4.1.0-8-flex_4.1.0.16076.swc \
-library-path+=libs/flexunit-aircilistener-4.1.0-8-4.1.0.16076.swc \
-library-path+=libs/flexunit-cilistener-4.1.0-8-4.1.0.16076.swc \
-library-path+=libs/flexunit-flexcoverlistener-4.1.0-8-4.1.0.16076.swc \
-library-path+=libs/flexunit-uilistener-4.1.0-8-4.1.0.16076.swc \
-library-path+=libs/fluint-extensions-4.1.0-8-4.1.0.16076.swc \
-define=CONFIG::LOGGING,false -define=CONFIG::VERSION,0.1.1 \
-accessible=false -actionscript-file-encoding=UTF-8 \
-allow-source-path-overlap=false -as3=true -benchmark=true -use-network=true \
-debug=false -incremental=false -output=../../dist/slon.fallback.swf \
src/Slon.as

# -library-path+=libs/flashls.swc \